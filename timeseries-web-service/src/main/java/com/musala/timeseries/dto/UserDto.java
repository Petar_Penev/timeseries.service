package com.musala.timeseries.dto;

import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class UserDto {

	@NotNull
	private String username;

	@NotNull
	@Size(min = 5)
	private char[] password;

	private Set<String> authorities;

	@NotNull
	private Boolean isEnabled;
}
