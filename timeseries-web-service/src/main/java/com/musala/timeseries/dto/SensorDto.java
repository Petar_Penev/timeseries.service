package com.musala.timeseries.dto;

import javax.validation.constraints.NotNull;

import com.musala.timeseries.common.SensorType;

import lombok.Data;

@Data
public class SensorDto {

	@NotNull
	private String name;

	@NotNull
	private SensorType type;

	@NotNull
	private String unit;
}
