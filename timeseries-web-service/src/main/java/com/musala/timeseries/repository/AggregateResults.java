package com.musala.timeseries.repository;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AggregateResults {

	private double average;
	private double min;
	private double max;
}