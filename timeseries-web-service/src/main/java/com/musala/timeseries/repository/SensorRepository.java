package com.musala.timeseries.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.musala.timeseries.entities.Sensor;

public interface SensorRepository extends CrudRepository<Sensor, String> {

	List<Sensor> getSensorByNameInAndUserUsername(List<String> names, String user);
}
