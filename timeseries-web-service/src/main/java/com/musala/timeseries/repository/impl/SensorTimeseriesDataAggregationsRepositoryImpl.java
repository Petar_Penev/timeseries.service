package com.musala.timeseries.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.Row;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.datastax.driver.core.querybuilder.Select.Selection;
import com.musala.timeseries.common.AggregationFunction;
import com.musala.timeseries.common.Precision;
import com.musala.timeseries.common.TimePeriod;
import com.musala.timeseries.entities.Sensor;
import com.musala.timeseries.repository.SensorRepository;
import com.musala.timeseries.repository.SensorTimeseriesDataAggregationsRepository;

@Repository
public class SensorTimeseriesDataAggregationsRepositoryImpl implements SensorTimeseriesDataAggregationsRepository {

	private static final String VALUE_COLUMN_TITLE = "value";
	private static final String NAME_COLUMN_TITLE = "name";
	private static final String TIME_COLUMN_TITLE = "time";

	@Value("${spring.data.cassandra.keyspace-name}")
	private String keyspace;

	@Value("${spring.data.cassandra.tables.finest}")
	private String finestTableName;

	@Value("${spring.data.cassandra.tables.hourly}")
	private String hourlyTableName;

	@Value("${spring.data.cassandra.tables.daily}")
	private String dailyTableName;

	private final CassandraOperations cassandraOperations;
	private final SensorRepository sensorRepository;

	@Autowired
	public SensorTimeseriesDataAggregationsRepositoryImpl(CassandraOperations cassandraOperations,
			SensorRepository sensorRepository) {
		this.cassandraOperations = cassandraOperations;
		this.sensorRepository = sensorRepository;
	}

	@Override
	public Map<String, Map<String, Double>> getTimeseriesForSensorsByAggregationFunctions(List<String> sensorNames,
			List<AggregationFunction> aggregations, Precision precision, TimePeriod timePeriod) {

		final Map<String, Map<String, Double>> result = new HashMap<>();

		final Selection selection = generateSelector(aggregations, precision);
		final String tableName = getTableName(precision);

		final List<Sensor> userSensors = getUserSensors(sensorNames);

		userSensors.forEach(sensor -> {
			final Select selectQuery = selection.from(keyspace, tableName)
												.where(QueryBuilder.eq(NAME_COLUMN_TITLE, sensor.getName()))
												.and(QueryBuilder.gte(TIME_COLUMN_TITLE, timePeriod.getMin()))
												.and(QueryBuilder.lte(TIME_COLUMN_TITLE, timePeriod.getMax()))
												.limit(1);

			final Row row = cassandraOperations.query(selectQuery).all().get(0);

			result.put(sensor.getName(), parseRow(row));
		});

		return result;
	}

	/**
	 * Gets sensors by names that are owned by the signed in user
	 * 
	 * @param sensorNames
	 *            the names of the sensors which should be get
	 */
	private List<Sensor> getUserSensors(List<String> sensorNames) {
		final String username = SecurityContextHolder.getContext().getAuthentication().getName();
		final List<Sensor> userSensors = sensorRepository.getSensorByNameInAndUserUsername(sensorNames, username);
		return userSensors;
	}

	/**
	 * Parses row object to Map<String, Double>
	 * 
	 * @param row
	 *            the row which should be parsed
	 */
	private Map<String, Double> parseRow(final Row row) {
		final Map<String, Double> resultValues = new HashMap<>();

		row.getColumnDefinitions().forEach(definition -> {
			final String functionName = definition.getName();
			resultValues.put(functionName, row.getDouble(functionName));
		});
		return resultValues;
	}

	/**
	 * Generates the select clause of the aggregations query
	 * 
	 * @param aggregations
	 *            desired aggregation functions which should be contained in the
	 *            select clause
	 * @param precision
	 *            the desired precision. It is needed because some of the tables are
	 *            pre-aggregated but other are not
	 */
	private Selection generateSelector(List<AggregationFunction> aggregations, Precision precision) {
		final boolean isDefaultColumn = Precision.FINEST.equals(precision);

		Selection selection = QueryBuilder.select();

		for (final AggregationFunction aggregation : aggregations) {
			final Object column = isDefaultColumn ? QueryBuilder.column(VALUE_COLUMN_TITLE)
					: QueryBuilder.column(aggregation.getValue());

			selection = selection.fcall(aggregation.getValue(), column).as(aggregation.getValue());
		}

		return selection;
	}

	private String getTableName(Precision precision) {
		switch (precision) {
		case FINEST:
			return finestTableName;
		case HOURLY:
			return hourlyTableName;
		case DAILY:
			return dailyTableName;
		default:
			throw new IllegalArgumentException(String.format("Precision [%s] has not been found.", precision));
		}
	}
}
