package com.musala.timeseries.repository;

import java.util.List;
import java.util.Map;

import com.musala.timeseries.common.AggregationFunction;
import com.musala.timeseries.common.Precision;
import com.musala.timeseries.common.TimePeriod;

public interface SensorTimeseriesDataAggregationsRepository {

	/**
	 * Gets timeseries data aggregated by aggregation functions
	 * 
	 * @param sensorNames
	 *            the sensor names for which the data is desired
	 * @param aggregationFunctions
	 *            Aggregation functions to be executed over the data
	 * @param precision
	 *            The precision of the data used for the calculations
	 * @param timePeriod
	 *            Time period for which the data to be calculated
	 */
	Map<String, Map<String, Double>> getTimeseriesForSensorsByAggregationFunctions(List<String> sensorNames,
			List<AggregationFunction> aggregationFunctions, Precision precision, TimePeriod timePeriod);
}
