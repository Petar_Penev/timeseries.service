package com.musala.timeseries.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.musala.timeseries.entities.Authority;

public interface AuthorityRepository extends CrudRepository<Authority, Long> {
	List<Authority> findByUserUsername(String username);

	void deleteByUserUsername(String username);
}
