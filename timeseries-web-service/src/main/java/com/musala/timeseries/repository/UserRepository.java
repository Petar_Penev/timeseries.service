package com.musala.timeseries.repository;

import org.springframework.data.repository.CrudRepository;

import com.musala.timeseries.entities.User;

public interface UserRepository extends CrudRepository<User, String> {
	User findByUsername(String username);
}
