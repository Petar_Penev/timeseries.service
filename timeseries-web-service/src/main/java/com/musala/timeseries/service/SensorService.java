package com.musala.timeseries.service;

import java.util.List;
import java.util.Map;

import com.musala.timeseries.common.AggregationFunction;
import com.musala.timeseries.common.Precision;
import com.musala.timeseries.common.TimePeriod;
import com.musala.timeseries.dto.SensorDto;
import com.musala.timeseries.exceptions.DuplicateSensorException;

public interface SensorService {

	/**
	 * Creates new sensor by the given sensor data
	 * 
	 * @param sensorDto
	 */
	void addSensor(SensorDto sensorDto) throws DuplicateSensorException;

	/**
	 * Gets timeseries data aggregated by aggregation functions
	 * 
	 * @param sensorNames
	 *            the sensor names for which the data is desired
	 * @param aggregationFunctions
	 *            Aggregation functions to be executed over the data
	 * @param precision
	 *            The precision of the data used for the calculations
	 * @param timePeriod
	 *            Time period for which the data to be calculated
	 */
	Map<String, Map<String, Double>> getSensorData(List<String> sensorNames,
			List<AggregationFunction> aggreagationFunctions, Precision precision, TimePeriod timePeriod);
}
