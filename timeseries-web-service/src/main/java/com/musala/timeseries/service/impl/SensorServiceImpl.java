package com.musala.timeseries.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.musala.timeseries.common.AggregationFunction;
import com.musala.timeseries.common.Precision;
import com.musala.timeseries.common.TimePeriod;
import com.musala.timeseries.dto.SensorDto;
import com.musala.timeseries.entities.Sensor;
import com.musala.timeseries.exceptions.DuplicateSensorException;
import com.musala.timeseries.mapper.SensorModelMapper;
import com.musala.timeseries.repository.SensorRepository;
import com.musala.timeseries.repository.SensorTimeseriesDataAggregationsRepository;
import com.musala.timeseries.service.SensorService;

@Service
public class SensorServiceImpl implements SensorService {

	private final SensorRepository sensorRepository;
	private final SensorModelMapper sensorModelMapper;

	@Autowired
	private SensorTimeseriesDataAggregationsRepository sensorTimeseriesDataAggregationsRepository;

	@Autowired
	public SensorServiceImpl(SensorRepository sensorRepository, SensorModelMapper sensorModelMapper) {
		super();
		this.sensorRepository = sensorRepository;
		this.sensorModelMapper = sensorModelMapper;
	}

	@Override
	public void addSensor(SensorDto sensorDto) throws DuplicateSensorException {
		final String username = SecurityContextHolder.getContext().getAuthentication().getName();
		final Sensor sensor = this.sensorModelMapper.map(username, sensorDto);

		if (sensorRepository.findOne(sensor.getName()) != null) {
			throw new DuplicateSensorException(
					String.format("Sensor with name [%s] already exists.", sensor.getName()));
		}

		sensorRepository.save(sensor);
	}

	@Override
	public Map<String, Map<String, Double>> getSensorData(List<String> sensorNames,
			List<AggregationFunction> aggreagationFunctions, Precision precision, TimePeriod timePeriod) {

		return sensorTimeseriesDataAggregationsRepository.getTimeseriesForSensorsByAggregationFunctions(sensorNames,
				aggreagationFunctions, precision, timePeriod);
	}
}
