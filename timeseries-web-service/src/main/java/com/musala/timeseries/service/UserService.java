package com.musala.timeseries.service;

import com.musala.timeseries.dto.UserDto;

public interface UserService {

	/**
	 * Saves an user by {@link UserDto}. If the user is not existing then creates
	 * new one but if it's existing then updates it. This method updates the
	 * authorities of the user too.
	 * 
	 * @param user
	 *            User to be updated
	 */
	void saveUser(UserDto user);

	/**
	 * Uses the default application password encoder and encodes the password of the
	 * DTO
	 * 
	 * @param user
	 *            User for which the password should be encoded
	 */
	void securePassword(UserDto user);
}
