package com.musala.timeseries.service.impl;

import java.nio.CharBuffer;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.musala.timeseries.dto.UserDto;
import com.musala.timeseries.entities.Authority;
import com.musala.timeseries.entities.User;
import com.musala.timeseries.mapper.AuthorityModelMapper;
import com.musala.timeseries.mapper.UserModelMapper;
import com.musala.timeseries.repository.AuthorityRepository;
import com.musala.timeseries.repository.UserRepository;
import com.musala.timeseries.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;
	private final AuthorityRepository authorityRepository;
	private final UserModelMapper userModelMapper;
	private final AuthorityModelMapper authorityModelMapper;
	private final PasswordEncoder passwordEncoder;

	public UserServiceImpl(UserRepository userRepository, UserModelMapper userModelMapper,
			AuthorityModelMapper authorityModelMapper, PasswordEncoder passwordEncoder,
			AuthorityRepository authorityRepository) {
		super();
		this.userRepository = userRepository;
		this.userModelMapper = userModelMapper;
		this.authorityModelMapper = authorityModelMapper;
		this.passwordEncoder = passwordEncoder;
		this.authorityRepository = authorityRepository;
	}

	@Override
	@Transactional
	public void saveUser(UserDto userDto) {
		final User user = userModelMapper.map(userDto);

		final List<Authority> authorities = authorityModelMapper.map(user, userDto.getAuthorities());
		user.setAuthorities(authorities);

		authorityRepository.deleteByUserUsername(user.getUsername());
		userRepository.save(user);
	}

	@Override
	public void securePassword(UserDto user) {
		final String encodedPassword = passwordEncoder.encode(CharBuffer.wrap(user.getPassword()));
		user.setPassword(encodedPassword.toCharArray());
	}
}
