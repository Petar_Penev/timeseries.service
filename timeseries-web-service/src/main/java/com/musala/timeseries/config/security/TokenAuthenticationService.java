package com.musala.timeseries.config.security;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.musala.timeseries.entities.Authority;
import com.musala.timeseries.repository.AuthorityRepository;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
class TokenAuthenticationService implements AuthenticationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TokenAuthenticationService.class);

	private static final long EXPIRATION_TIME = TimeUnit.DAYS.toMillis(10);
	private static final String TOKEN_PREFIX = "Bearer";
	private static final String HEADER_STRING = "Authorization";

	@Value("${spring.security.jwt}")
	private String jwtSecret;

	private final AuthorityRepository authorityRepository;

	@Autowired
	public TokenAuthenticationService(AuthorityRepository authorityRepository) {
		this.authorityRepository = authorityRepository;
	}

	@Override
	public void addAuthentication(HttpServletResponse response, Authentication auth) throws JsonProcessingException {

		final String JWT = Jwts	.builder()
								.setSubject(auth.getName())
								.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
								.signWith(SignatureAlgorithm.HS512, jwtSecret)
								.compact();
		response.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
	}

	@Override
	public Authentication getAuthentication(HttpServletRequest request) {
		try {
			final String token = request.getHeader(HEADER_STRING);
			if (token != null) {
				final String user = Jwts.parser()
										.setSigningKey(jwtSecret)
										.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
										.getBody()
										.getSubject();

				final List<JWTGrantedAuthority> authorities = getAuthorities(user);

				return user != null ? new UsernamePasswordAuthenticationToken(user, null, authorities) : null;
			}
		} catch (final Exception e) {
			LOGGER.error("Problem occured while trying to authenticate user: ", e);
		}
		return null;
	}

	private List<JWTGrantedAuthority> getAuthorities(String username) {
		final List<Authority> authorities = this.authorityRepository.findByUserUsername(username);

		return authorities.stream().map(authority -> new JWTGrantedAuthority(authority.getAuthority())).collect(
				Collectors.toList());
	}
}