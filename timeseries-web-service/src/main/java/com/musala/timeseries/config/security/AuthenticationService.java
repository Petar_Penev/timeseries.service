package com.musala.timeseries.config.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface AuthenticationService {

	Authentication getAuthentication(HttpServletRequest request);

	void addAuthentication(HttpServletResponse response, Authentication auth) throws JsonProcessingException;
}
