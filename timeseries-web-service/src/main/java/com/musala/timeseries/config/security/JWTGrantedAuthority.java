package com.musala.timeseries.config.security;

import org.springframework.security.core.GrantedAuthority;

public class JWTGrantedAuthority implements GrantedAuthority {

	private static final long serialVersionUID = -1950200542614864690L;

	private final String authority;

	public JWTGrantedAuthority(String authority) {
		super();
		this.authority = authority;
	}

	@Override
	public String getAuthority() {
		return authority;
	}

}
