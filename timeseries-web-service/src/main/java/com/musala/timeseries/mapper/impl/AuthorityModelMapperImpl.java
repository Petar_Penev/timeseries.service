package com.musala.timeseries.mapper.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.musala.timeseries.entities.Authority;
import com.musala.timeseries.entities.User;
import com.musala.timeseries.mapper.AuthorityModelMapper;

@Component
public class AuthorityModelMapperImpl implements AuthorityModelMapper {

	@Override
	public Authority map(User user, String authorityValue) {
		final Authority authority = new Authority();
		authority.setAuthority(authorityValue);
		authority.setUser(user);

		return authority;
	}

	@Override
	public List<Authority> map(User user, Set<String> authorityValues) {
		return authorityValues.stream().map(authorityValue -> this.map(user, authorityValue)).collect(
				Collectors.toList());
	}

}
