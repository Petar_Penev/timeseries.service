package com.musala.timeseries.mapper;

import java.util.List;
import java.util.Set;

import com.musala.timeseries.entities.Authority;
import com.musala.timeseries.entities.User;

public interface AuthorityModelMapper {
	Authority map(User user, String authority);

	List<Authority> map(User user, Set<String> authority);
}
