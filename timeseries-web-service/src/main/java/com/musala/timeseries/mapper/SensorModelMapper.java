package com.musala.timeseries.mapper;

import com.musala.timeseries.dto.SensorDto;
import com.musala.timeseries.entities.Sensor;

public interface SensorModelMapper {
	Sensor map(String username, SensorDto sensorDto);
}
