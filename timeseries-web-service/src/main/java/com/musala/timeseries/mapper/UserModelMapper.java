package com.musala.timeseries.mapper;

import com.musala.timeseries.dto.UserDto;
import com.musala.timeseries.entities.User;

public interface UserModelMapper {
	User map(UserDto userDto);
}
