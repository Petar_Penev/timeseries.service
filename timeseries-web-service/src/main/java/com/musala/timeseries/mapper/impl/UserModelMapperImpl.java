package com.musala.timeseries.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.musala.timeseries.dto.UserDto;
import com.musala.timeseries.entities.User;
import com.musala.timeseries.mapper.UserModelMapper;

@Component
public class UserModelMapperImpl implements UserModelMapper {

	private final ModelMapper modelMapper;

	@Autowired
	public UserModelMapperImpl(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

	@Override
	public User map(UserDto userDto) {
		return modelMapper.map(userDto, User.class);
	}
}
