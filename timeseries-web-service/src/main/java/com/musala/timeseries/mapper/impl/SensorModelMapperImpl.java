package com.musala.timeseries.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.musala.timeseries.dto.SensorDto;
import com.musala.timeseries.entities.Sensor;
import com.musala.timeseries.entities.User;
import com.musala.timeseries.mapper.SensorModelMapper;
import com.musala.timeseries.repository.UserRepository;

@Component
public class SensorModelMapperImpl implements SensorModelMapper {

	private final ModelMapper modelMapper;
	private final UserRepository userRepository;

	@Autowired
	public SensorModelMapperImpl(ModelMapper modelMapper, UserRepository userRepository) {
		this.modelMapper = modelMapper;
		this.userRepository = userRepository;
	}

	@Override
	public Sensor map(String username, SensorDto sensorDto) {
		final Sensor sensor = modelMapper.map(sensorDto, Sensor.class);

		final User user = userRepository.findByUsername(username);
		sensor.setUser(user);

		return sensor;
	}
}
