package com.musala.timeseries.controller;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.musala.timeseries.common.AggregationConverter;
import com.musala.timeseries.common.AggregationFunction;
import com.musala.timeseries.common.Precision;
import com.musala.timeseries.common.PrecisionConverter;
import com.musala.timeseries.common.TimePeriod;
import com.musala.timeseries.dto.SensorDto;
import com.musala.timeseries.exceptions.DuplicateSensorException;
import com.musala.timeseries.exceptions.TimePeriodException;
import com.musala.timeseries.service.SensorService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

@RestController
@RequestMapping(path = "/v1/sensors")
public class SensorController {

	private final SensorService sensorService;

	@Autowired
	public SensorController(SensorService sensorService) {
		this.sensorService = sensorService;
	}

	@ApiResponses(value = { @ApiResponse(code = 201, message = "Sensor is successfully created"),
			@ApiResponse(code = 409, message = "Sensor has been already existing") })
	@ApiOperation(value = "Creates new sensor", notes = "The sensors are unique by name.",
			authorizations = { @Authorization(value = "petoauth", scopes = {
					@AuthorizationScope(scope = "SENSOR_ADD", description = "allows adding new sensors") }) })
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasAuthority('SENSOR_ADD')")
	public void addNewSensor(@RequestBody @Valid SensorDto sensorDto) throws DuplicateSensorException {
		sensorService.addSensor(sensorDto);
	}

	@ApiOperation(value = "Returns aggregations over the data of the given sensors",
			notes = "Multiple parameters can be passed for better results such as aggregations, precision, minTime and maxTime.",
			authorizations = { @Authorization(value = "petoauth", scopes = {
					@AuthorizationScope(scope = "SENSOR_DATA", description = "allows retrieving sensors' data") }) })
	@GetMapping("/{sensorNames}/data")
	@PreAuthorize("hasAuthority('SENSOR_DATA')")
	public Map<String, Map<String, Double>> getSensorData(@PathVariable List<String> sensorNames,
			@ApiParam(value = "Aggregations is used to define which aggregation functions to be executed over the data",
					defaultValue = "min,max,avg",
					required = false) @RequestParam(required = false) List<AggregationFunction> aggreagations,
			@ApiParam(value = "Precision is the precision of the calculation", defaultValue = "hourly",
					required = false) @RequestParam(required = false) Precision precision,
			@ApiParam(value = "MinTime is the begining of the period for which the calculation will be made",
					defaultValue = "24 hours before the time of the request",
					required = false) @RequestParam(required = false) Long minTime,
			@ApiParam(value = "MaxTime is the end of the period for which the calculation will be made",
					defaultValue = "the time of the request",
					required = false) @RequestParam(required = false) Long maxTime)
			throws TimePeriodException {

		if (aggreagations == null || aggreagations.isEmpty()) {
			aggreagations = Arrays.asList(AggregationFunction.values());
		}

		if (precision == null) {
			precision = Precision.HOURLY;
		}

		final TimePeriod timePeriod = new TimePeriod(minTime, maxTime);
		if (Precision.FINEST.equals(precision) && !isWithinWeekAgo(timePeriod.getZonedMin())) {
			throw new TimePeriodException(
					String.format("When using precision [%s] the time period should be within a week ago",
							Precision.FINEST.getValue()));
		}

		return sensorService.getSensorData(sensorNames, aggreagations, precision, timePeriod);
	}

	private boolean isWithinWeekAgo(ZonedDateTime zonedDateTime) {
		final ZonedDateTime oneWeekAgo = ZonedDateTime.now(ZoneOffset.UTC).minusWeeks(1);
		return oneWeekAgo.isBefore(zonedDateTime);
	}

	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		dataBinder.registerCustomEditor(AggregationFunction.class, new AggregationConverter());
		dataBinder.registerCustomEditor(Precision.class, new PrecisionConverter());
	}
}
