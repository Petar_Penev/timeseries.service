package com.musala.timeseries.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.musala.timeseries.dto.UserDto;
import com.musala.timeseries.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

@RestController
@RequestMapping(path = "/v1/users")
public class UserController {

	private final UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@ApiOperation(value = "Creates new user or updates already existing one with given authorities and password",
			notes = "To manage users the signed in user should own ADMIN role.",
			authorizations = { @Authorization(value = "petoauth",
					scopes = { @AuthorizationScope(scope = "ROLE_ADMIN", description = "allows admin functions") }) })
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasRole('ADMIN')")
	public void saveUser(@RequestBody @Valid UserDto userDto) {
		userService.securePassword(userDto);
		userService.saveUser(userDto);
	}
}
