package com.musala.timeseries.common;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.TimeZone;

public class TimePeriod {

	private static final ZoneId utcZoneId = TimeZone.getTimeZone("UTC").toZoneId();

	private final ZonedDateTime min;
	private final ZonedDateTime max;

	/**
	 * Creates new TimePeriod object. If min or max is null then default value is
	 * used. The default value of min is 24 hours before the creation of the object
	 * but for max is the moment of creation.
	 * 
	 * @param min
	 *            the beginning of the period
	 * @param max
	 *            the end of the period
	 */
	public TimePeriod(Long min, Long max) {
		super();

		if (min == null) {
			this.min = ZonedDateTime.now(ZoneOffset.UTC).minusDays(1);
		} else {
			this.min = ZonedDateTime.ofInstant(Instant.ofEpochMilli(min), utcZoneId);
		}

		if (max == null) {
			this.max = ZonedDateTime.now(ZoneOffset.UTC);
		} else {
			this.max = ZonedDateTime.ofInstant(Instant.ofEpochMilli(max), utcZoneId);
		}

		if (this.max.isBefore(this.min)) {
			throw new IllegalArgumentException("Max time cannot be before the min time.");
		}
	}

	public ZonedDateTime getZonedMin() {
		return min;
	}

	public ZonedDateTime getZonedMax() {
		return max;
	}

	public Date getMin() {
		return Date.from(min.toInstant());
	}

	public Date getMax() {
		return Date.from(max.toInstant());
	}
}
