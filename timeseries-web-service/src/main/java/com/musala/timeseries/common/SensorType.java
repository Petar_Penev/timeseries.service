package com.musala.timeseries.common;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum SensorType {
	THERMOSTAT("thermostat"), HEART_RATE_METER("hear_rate_meter"), CAR_FUEL_MONITOR("car_fuel_monitor");

	private String type;

	private SensorType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	@JsonCreator
	public static SensorType fromString(String type) {
		for (final SensorType sensorType : SensorType.values()) {
			if (sensorType.getType().equals(type)) {
				return sensorType;
			}
		}

		throw new IllegalArgumentException(String.format("Sensor type [%s] is not existing", type));
	}

	@Override
	public String toString() {
		return type;
	}
}
