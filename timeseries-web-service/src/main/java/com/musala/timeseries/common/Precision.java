package com.musala.timeseries.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Precision {
	FINEST("finest"), HOURLY("hourly"), DAILY("daily");

	private String value;

	private Precision(String value) {
		this.value = value;
	}

	@JsonCreator
	public static Precision fromString(String value) {
		for (final Precision precision : Precision.values()) {
			if (precision.getValue().equals(value)) {
				return precision;
			}
		}
		throw new IllegalArgumentException(String.format("Precision value [%s] is not existing", value));
	}

	public String getValue() {
		return value;
	}

	@JsonValue
	@Override
	public String toString() {
		return value;
	}
}
