package com.musala.timeseries.common;

import java.beans.PropertyEditorSupport;

public class AggregationConverter extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text == null) {
			return;
		}

		setValue(AggregationFunction.fromString(text.toLowerCase()));
	}
}
