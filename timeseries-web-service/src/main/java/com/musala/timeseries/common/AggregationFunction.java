package com.musala.timeseries.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum AggregationFunction {

	MIN("min"), MAX("max"), AVERAGE("avg");

	private String value;

	private AggregationFunction(String value) {
		this.value = value;
	}

	@JsonCreator
	public static AggregationFunction fromString(String value) {
		for (final AggregationFunction aggregation : AggregationFunction.values()) {
			if (aggregation.getValue().equals(value)) {
				return aggregation;
			}
		}
		throw new IllegalArgumentException(String.format("Aggregation function [%s] is not existing", value));
	}

	public String getValue() {
		return this.value;
	}

	@JsonValue
	@Override
	public String toString() {
		return value;
	}
}
