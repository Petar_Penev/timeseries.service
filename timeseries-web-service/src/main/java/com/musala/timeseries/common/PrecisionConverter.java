package com.musala.timeseries.common;

import java.beans.PropertyEditorSupport;

public class PrecisionConverter extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text == null) {
			return;
		}

		setValue(Precision.fromString(text.toLowerCase()));
	}
}
