package com.musala.timeseries.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "Users")
public class User implements Serializable {

	private static final long serialVersionUID = -1646541577658821862L;

	@Id
	@NotNull
	private String username;

	@Column
	@NotNull
	private String password;

	@NotNull
	@Column(name = "enabled")
	private boolean isEnabled;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "user")
	private List<Authority> authorities;
}