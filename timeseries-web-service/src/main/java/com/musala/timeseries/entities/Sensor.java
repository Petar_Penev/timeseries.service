package com.musala.timeseries.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "Sensors")
public class Sensor implements Serializable {

	private static final long serialVersionUID = -1646541577658821862L;

	@Id
	@NotNull
	@Column(unique = true)
	private String name;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "user")
	private User user;

	@Column
	@NotNull
	private String type;

	@Column
	@NotNull
	private String unit;
}