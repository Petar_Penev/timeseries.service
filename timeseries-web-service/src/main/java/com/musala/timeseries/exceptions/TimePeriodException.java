package com.musala.timeseries.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class TimePeriodException extends Exception {

	private static final long serialVersionUID = 6190037268626593894L;

	public TimePeriodException(String message) {
		super(message);
	}
}
