package com.musala.timeseries.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DuplicateSensorException extends Exception {

	private static final long serialVersionUID = -7110783187787338367L;

	public DuplicateSensorException(String message) {
		super(message);
	}
}
