package com.musala.timeseries.mapper;

import com.musala.timeseries.dto.SensorData;

public interface DataMapper<T> {
	SensorData map(T sensorData);
}
