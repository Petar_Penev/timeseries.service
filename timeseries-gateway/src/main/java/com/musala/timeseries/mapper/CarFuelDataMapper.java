package com.musala.timeseries.mapper;

import org.springframework.stereotype.Component;

import com.musala.timeseries.dto.CarFuelMonitorData;
import com.musala.timeseries.dto.SensorData;

@Component
public class CarFuelDataMapper implements DataMapper<CarFuelMonitorData> {

	@Override
	public SensorData map(CarFuelMonitorData carFuelMonitorData) {
		return new SensorData(carFuelMonitorData.getId(), carFuelMonitorData.getVolume(), carFuelMonitorData.getTime());
	}
}
