package com.musala.timeseries.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.musala.timeseries.dto.SensorData;
import com.musala.timeseries.dto.ThermostatData;

@Component
public class ThermostatSensorDataMapper implements DataMapper<ThermostatData> {

	private final ModelMapper modelMapper;

	@Autowired
	public ThermostatSensorDataMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

	@Override
	public SensorData map(ThermostatData thermostatData) {
		final SensorData sensorData = modelMapper.map(thermostatData, SensorData.class);
		sensorData.setValue(thermostatData.getTemperature());

		return sensorData;
	}
}
