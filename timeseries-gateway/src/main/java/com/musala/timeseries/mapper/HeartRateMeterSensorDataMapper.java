package com.musala.timeseries.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.musala.timeseries.dto.HeartRateMeterData;
import com.musala.timeseries.dto.SensorData;

@Component
public class HeartRateMeterSensorDataMapper implements DataMapper<HeartRateMeterData> {

	private final ModelMapper modelMapper;

	@Autowired
	public HeartRateMeterSensorDataMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

	@Override
	public SensorData map(HeartRateMeterData hearRateMonitor) {
		final SensorData sensorData = modelMapper.map(hearRateMonitor, SensorData.class);
		return sensorData;
	}
}
