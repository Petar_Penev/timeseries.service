package com.musala.timeseries.producer;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.musala.timeseries.connector.CarFuelMonitorConnector;
import com.musala.timeseries.connector.HearRateMeterConnector;
import com.musala.timeseries.connector.ThermostatConnector;
import com.musala.timeseries.dto.CarFuelMonitorData;
import com.musala.timeseries.dto.HeartRateMeterData;
import com.musala.timeseries.dto.ThermostatData;

/**
 * This class mocks the sensors
 * 
 * @author petar.penev
 */
@Component
public class Producer {
	private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);

	private static final long ONE_SECOND = TimeUnit.SECONDS.toMillis(1);

	final Random random = new Random();
	private final ThermostatConnector thermostatConnector;
	private final CarFuelMonitorConnector carFuelMonitorConnector;
	private final HearRateMeterConnector hearRateMeterConnector;

	@Autowired
	public Producer(ThermostatConnector thermostatConnector, CarFuelMonitorConnector carFuelMonitorConnector,
			HearRateMeterConnector hearRateMeterConnector) {
		this.thermostatConnector = thermostatConnector;
		this.carFuelMonitorConnector = carFuelMonitorConnector;
		this.hearRateMeterConnector = hearRateMeterConnector;
	}

	@PostConstruct
	public void runSensors() {

		executeEverySecond(() -> {
			final double temperature = randomDoubleBetween(-10, 30);
			final ThermostatData thermostatData = new ThermostatData("thermostat_1", temperature, utc());
			LOGGER.debug(thermostatData.toString());

			thermostatConnector.receiveData(thermostatData);
		});

		executeEverySecond(() -> {
			final double volume = randomDoubleBetween(0, 100);
			final CarFuelMonitorData carFuelMonitorData = new CarFuelMonitorData("car_fuel_1", volume, utc());

			LOGGER.debug(carFuelMonitorData.toString());

			this.carFuelMonitorConnector.receiveData(carFuelMonitorData);
		});

		// one day behind
		executeEverySecond(() -> {
			final double temperature = randomDoubleBetween(-20, 20);

			final ThermostatData thermostatData = new ThermostatData("thermostat_2", temperature,
					utc() - TimeUnit.DAYS.toMillis(1));

			LOGGER.debug(thermostatData.toString());

			thermostatConnector.receiveData(thermostatData);
		});

		// one hour behind
		executeEverySecond(() -> {
			final double hearRate = randomIntegerBetween(60, 150);

			final HeartRateMeterData heartRateMeterData = new HeartRateMeterData("hrm_1", hearRate,
					utc() - TimeUnit.HOURS.toMillis(1));

			LOGGER.debug(heartRateMeterData.toString());

			hearRateMeterConnector.receiveData(heartRateMeterData);
		});

		simulateHearRateMonitors("hrm_bosch_", 100);
	}

	private void simulateHearRateMonitors(String namePrefix, int count) {
		IntStream.range(0, count).forEach((index) -> {
			executeEverySecond(() -> {
				final double hearRate = randomIntegerBetween(60, 150);

				final HeartRateMeterData heartRateMeterData = new HeartRateMeterData(
						"hrm_bosch_".concat(String.valueOf(index)), hearRate, utc());

				LOGGER.debug(heartRateMeterData.toString());

				hearRateMeterConnector.receiveData(heartRateMeterData);

			});
		});
	}

	private void executeEverySecond(Actionable actionable) {
		new Thread(() -> {
			while (true) {

				actionable.action();

				try {
					Thread.sleep(ONE_SECOND);
				} catch (final InterruptedException e) {
					LOGGER.error("Error occured while trying to sleep thread.", e);
				}
			}
		}).start();
	}

	private double randomDoubleBetween(int min, int max) {
		return random.nextInt(max + 1 - min) + min + random.nextDouble();
	}

	private int randomIntegerBetween(int min, int max) {
		return random.nextInt(max + 1 - min) + min;
	}

	private long utc() {
		return ZonedDateTime.now(ZoneOffset.UTC).toInstant().toEpochMilli();
	}
}
