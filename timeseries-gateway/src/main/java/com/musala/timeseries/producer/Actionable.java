package com.musala.timeseries.producer;

@FunctionalInterface
public interface Actionable {
	void action();
}
