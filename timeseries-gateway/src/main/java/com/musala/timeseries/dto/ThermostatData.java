package com.musala.timeseries.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ThermostatData {

	private final String name;
	private final Double temperature;
	private final Long time;
}
