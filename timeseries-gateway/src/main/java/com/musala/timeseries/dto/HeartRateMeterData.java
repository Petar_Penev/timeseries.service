package com.musala.timeseries.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HeartRateMeterData {

	private String name;
	private Double value;
	private Long time;
}
