package com.musala.timeseries.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CarFuelMonitorData {

	private String id;
	private Double volume;
	private Long time;
}
