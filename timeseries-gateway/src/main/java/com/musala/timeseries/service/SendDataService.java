package com.musala.timeseries.service;

import com.musala.timeseries.dto.SensorData;

public interface SendDataService {
	/**
	 * Sends sensor data to the data retriever service
	 * 
	 * @param sensorData
	 */
	void sendSensorData(SensorData sensorData);
}
