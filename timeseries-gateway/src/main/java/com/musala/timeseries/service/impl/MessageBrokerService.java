package com.musala.timeseries.service.impl;

import javax.jms.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.musala.timeseries.dto.SensorData;
import com.musala.timeseries.service.SendDataService;

@Service
public class MessageBrokerService implements SendDataService {
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageBrokerService.class);

	private final JmsMessagingTemplate jmsMessagingTemplate;
	private final Queue queue;
	private final ObjectMapper objectMapper = new ObjectMapper();;

	@Autowired
	public MessageBrokerService(JmsMessagingTemplate jmsMessagingTemplate, Queue queue) {
		this.jmsMessagingTemplate = jmsMessagingTemplate;
		this.queue = queue;
	}

	@Override
	public void sendSensorData(SensorData sensorData) {
		try {
			final String message = objectMapper.writeValueAsString(sensorData);
			this.jmsMessagingTemplate.convertAndSend(this.queue, message);
		} catch (final JsonProcessingException e) {
			LOGGER.error("Exception occured while trying to serialize sensor data [{}]", sensorData.toString(), e);
		}

	}
}
