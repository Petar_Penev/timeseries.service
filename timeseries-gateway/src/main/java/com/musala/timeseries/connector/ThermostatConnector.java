package com.musala.timeseries.connector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.musala.timeseries.dto.SensorData;
import com.musala.timeseries.dto.ThermostatData;
import com.musala.timeseries.mapper.ThermostatSensorDataMapper;
import com.musala.timeseries.service.SendDataService;

@Component
public class ThermostatConnector {

	private final ThermostatSensorDataMapper thermostatSensorDataMapper;
	private final SendDataService sendDataService;

	@Autowired
	public ThermostatConnector(ThermostatSensorDataMapper thermostatSensorDataMapper, SendDataService sendDataService) {
		this.thermostatSensorDataMapper = thermostatSensorDataMapper;
		this.sendDataService = sendDataService;
	}

	public void receiveData(ThermostatData thermostatData) {
		final SensorData sensorData = thermostatSensorDataMapper.map(thermostatData);
		sendDataService.sendSensorData(sensorData);
	}
}
