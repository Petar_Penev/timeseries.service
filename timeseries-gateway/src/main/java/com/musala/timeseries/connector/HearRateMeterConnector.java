package com.musala.timeseries.connector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.musala.timeseries.dto.HeartRateMeterData;
import com.musala.timeseries.dto.SensorData;
import com.musala.timeseries.mapper.HeartRateMeterSensorDataMapper;
import com.musala.timeseries.service.SendDataService;

@Component
public class HearRateMeterConnector {

	private final HeartRateMeterSensorDataMapper hearRateMeterSensorDataMapper;
	private final SendDataService sendDataService;

	@Autowired
	public HearRateMeterConnector(HeartRateMeterSensorDataMapper hearRateMeterSensorDataMapper,
			SendDataService sendDataService) {
		this.hearRateMeterSensorDataMapper = hearRateMeterSensorDataMapper;
		this.sendDataService = sendDataService;
	}

	public void receiveData(HeartRateMeterData hearRateMeterData) {
		final SensorData sensorData = hearRateMeterSensorDataMapper.map(hearRateMeterData);
		sendDataService.sendSensorData(sensorData);
	}
}
