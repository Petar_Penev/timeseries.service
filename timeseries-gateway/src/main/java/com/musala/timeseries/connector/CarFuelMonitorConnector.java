package com.musala.timeseries.connector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.musala.timeseries.dto.CarFuelMonitorData;
import com.musala.timeseries.dto.SensorData;
import com.musala.timeseries.mapper.CarFuelDataMapper;
import com.musala.timeseries.service.SendDataService;

@Component
public class CarFuelMonitorConnector {

	private final CarFuelDataMapper carFuelDataMapper;
	private final SendDataService sendDataService;

	@Autowired
	public CarFuelMonitorConnector(CarFuelDataMapper carFuelDataMapper, SendDataService sendDataService) {
		this.carFuelDataMapper = carFuelDataMapper;
		this.sendDataService = sendDataService;
	}

	public void receiveData(CarFuelMonitorData data) {
		final SensorData sensorData = carFuelDataMapper.map(data);
		sendDataService.sendSensorData(sensorData);
	}
}
