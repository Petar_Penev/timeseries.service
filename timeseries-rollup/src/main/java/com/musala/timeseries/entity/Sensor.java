package com.musala.timeseries.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Sensors")
public class Sensor implements Serializable {

	private static final long serialVersionUID = -1646541577658821862L;

	@Id
	@Column
	private String name;

	@Column
	private String user;

	@Column
	private String type;

	@Column
	private String unit;
}