package com.musala.timeseries.repository.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.musala.timeseries.common.AggregationFuntion;
import com.musala.timeseries.repository.RollupRepository;

@Repository
public class RollupRepositoryImpl implements RollupRepository {

	private final Session session;

	// Prepare statements are used for performance optimization
	private final PreparedStatement getAggregations;
	private final PreparedStatement insertHourlyAggregationsData;
	private final PreparedStatement insertDailyAggregationsData;

	@Autowired
	public RollupRepositoryImpl(Session session) {
		super();
		this.session = session;

		this.getAggregations = session.prepare(String.format(
				"select name, avg(value) as %s, min(value) as %s, max(value) as %s from sensordata "
						+ "where name=? and time>=? and time<=?",
				AggregationFuntion.AVG, AggregationFuntion.MIN, AggregationFuntion.MAX));

		this.insertHourlyAggregationsData = session.prepare(
				"insert into sensor_timeseries.sensorData_hourly(name, time, min, max, avg) values (?, ?, ?, ?, ?)");

		this.insertDailyAggregationsData = session.prepare(
				"insert into sensor_timeseries.sensorData_daily(name, time, min, max, avg) values (?, ?, ?, ?, ?)");
	}

	@Override
	public Row getAggregatedDataForPeriod(String name, Date from, Date to) {
		return session.execute(getAggregations.bind(name, from, to)).all().get(0);
	}

	@Override
	public void insertHourlyPrecisionAggregatedData(String name, Date time, double min, double max, double avg) {
		session.execute(insertHourlyAggregationsData.bind(name, time, min, max, avg));
	}

	@Override
	public void insertDailyPrecisionAggregatedData(String name, Date time, double min, double max, double avg) {
		session.execute(insertDailyAggregationsData.bind(name, time, min, max, avg));
	}
}
