package com.musala.timeseries.repository;

import org.springframework.data.repository.CrudRepository;

import com.musala.timeseries.entity.Sensor;

public interface SensorRepository extends CrudRepository<Sensor, Integer> {

}
