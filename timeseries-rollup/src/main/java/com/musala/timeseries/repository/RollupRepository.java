package com.musala.timeseries.repository;

import java.util.Date;

import com.datastax.driver.core.Row;

public interface RollupRepository {

	/**
	 * Returns the timeseries data for a particular sensor aggregated by min, max
	 * and average aggregation functions for a given period of time
	 * 
	 * @param sensorName
	 *            The name of the sensor for which the data will be retrieved
	 * @param from
	 *            The beginning of the time period
	 * @param to
	 *            The end of the time period
	 */
	Row getAggregatedDataForPeriod(String sensorName, Date from, Date to);

	/**
	 * Inserts the aggregated sensor data in the table with data precision of hour
	 * 
	 * @param sensorName
	 *            The name of the sensor for which the data will be inserted
	 * @param time
	 *            The time of the record. The time is with precision of hour
	 *            (minutes, seconds and milliseconds are stripped)
	 * @param min
	 *            The result of the aggregation with the min function
	 * @param max
	 *            The result of the aggregation with the max function
	 * @param avg
	 *            The result of the aggregation with the average function
	 */
	void insertHourlyPrecisionAggregatedData(String sensorName, Date time, double min, double max, double avg);

	/**
	 * Inserts the aggregated sensor data in the table with data precision of day
	 * 
	 * @param sensorName
	 *            The name of the sensor for which the data will be inserted
	 * @param time
	 *            The time of the record. The time is with precision of day
	 *            (minutes, seconds and milliseconds are stripped)
	 * @param min
	 *            The result of the aggregation with the min function
	 * @param max
	 *            The result of the aggregation with the max function
	 * @param avg
	 *            The result of the aggregation with the average function
	 */
	void insertDailyPrecisionAggregatedData(String sensorName, Date time, double min, double max, double avg);

}
