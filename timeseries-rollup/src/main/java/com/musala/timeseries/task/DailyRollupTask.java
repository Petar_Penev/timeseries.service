package com.musala.timeseries.task;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.datastax.driver.core.Row;
import com.musala.timeseries.common.AggregationFuntion;
import com.musala.timeseries.repository.RollupRepository;
import com.musala.timeseries.repository.SensorRepository;

@Component
public class DailyRollupTask {
	private static final Logger LOGGER = LoggerFactory.getLogger(DailyRollupTask.class);

	private static final String NAME_COLUMN_TITLE = "name";

	SensorRepository sensorRepository;
	RollupRepository rollupRepository;

	@Autowired
	public DailyRollupTask(SensorRepository sensorRepository, RollupRepository rollupRepository) {
		super();
		this.sensorRepository = sensorRepository;
		this.rollupRepository = rollupRepository;
	}

	// Every day at 4:00 AM
	// @Scheduled(cron = "* * 4 * * *")
	@Scheduled(cron = "*/30 * * * * *")
	public void rollupDays() {
		LOGGER.debug("Hourly debug has started at [{}] server time", new Date());

		final ZonedDateTime today = LocalDate.now().atStartOfDay().atZone(ZoneId.of("UTC"));
		final ZonedDateTime yesterday = today.minusDays(1);

		sensorRepository.findAll().forEach((sensor) -> {
			final String sensorName = sensor.getName();
			final Row aggregations = rollupRepository.getAggregatedDataForPeriod(sensorName, toDate(yesterday),
					toDate(today));

			// this means that there are no records for this sensor name in the time period
			if (aggregations.getString(NAME_COLUMN_TITLE) == null) {
				return;
			}

			final double min = aggregations.getDouble(AggregationFuntion.MIN);
			final double max = aggregations.getDouble(AggregationFuntion.MAX);
			final double avg = aggregations.getDouble(AggregationFuntion.AVG);

			rollupRepository.insertDailyPrecisionAggregatedData(sensorName, toDate(yesterday), min, max, avg);
		});

		LOGGER.debug("Hourly debug has been finished at [{}] server time", new Date());
	}

	private Date toDate(ZonedDateTime time) {
		return Date.from(time.toInstant());
	}
}
