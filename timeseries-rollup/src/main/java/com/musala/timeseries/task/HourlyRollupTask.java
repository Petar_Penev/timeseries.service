package com.musala.timeseries.task;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.datastax.driver.core.Row;
import com.musala.timeseries.common.AggregationFuntion;
import com.musala.timeseries.repository.RollupRepository;
import com.musala.timeseries.repository.SensorRepository;

@Component
public class HourlyRollupTask {
	private static final Logger LOGGER = LoggerFactory.getLogger(HourlyRollupTask.class);

	private static final String NAME_COLUMN_TITLE = "name";

	private final RollupRepository rollupRepository;
	private final SensorRepository sensorRepository;

	@Autowired
	public HourlyRollupTask(SensorRepository sensorRepository, RollupRepository rollupRepository) {
		super();
		this.sensorRepository = sensorRepository;
		this.rollupRepository = rollupRepository;
	}

	// Every hour at 30 minutes. Ex. ( 01:30, 02:30, 03:30 ...)
	// @Scheduled(cron = "* 30 * * * *")
	@Scheduled(cron = "*/30 * * * * *")
	public void rollupHours() {
		LOGGER.debug("Hourly debug has started at [{}] server time", new Date());

		final ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC).withMinute(0).withSecond(0).withNano(0);
		final ZonedDateTime hourAgo = now.minusHours(1);

		sensorRepository.findAll().forEach((sensor) -> {
			final String sensorName = sensor.getName();
			final Row aggregations = rollupRepository.getAggregatedDataForPeriod(sensorName, toDate(hourAgo),
					toDate(now));

			// this means that there are no records for this sensor name in the time period
			if (aggregations.getString(NAME_COLUMN_TITLE) == null) {
				return;
			}

			final double min = aggregations.getDouble(AggregationFuntion.MIN);
			final double max = aggregations.getDouble(AggregationFuntion.MAX);
			final double avg = aggregations.getDouble(AggregationFuntion.AVG);

			rollupRepository.insertHourlyPrecisionAggregatedData(sensorName, toDate(hourAgo), min, max, avg);
		});

		LOGGER.debug("Hourly debug has been finished at [{}] server time", new Date());
	}

	private static Date toDate(ZonedDateTime time) {
		return Date.from(time.toInstant());
	}
}
