package com.musala.timeseries.dto;

import lombok.Data;

@Data
public class SensorDataDto {
	private String name;
	private Double value;
	private Long time;
}
