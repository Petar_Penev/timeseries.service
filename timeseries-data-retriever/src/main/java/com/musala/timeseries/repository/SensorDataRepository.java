package com.musala.timeseries.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;

import com.musala.timeseries.entities.SensorData;

public interface SensorDataRepository extends CassandraRepository<SensorData> {
}
