package com.musala.timeseries.service;

import com.musala.timeseries.dto.SensorDataDto;

public interface SensorDataService {
	/**
	 * Saves the sensor data in the database
	 * 
	 * @param sensorDataDto
	 *            The data that will be recorded in the database
	 */
	void saveSensorData(SensorDataDto sensorDataDto);
}
