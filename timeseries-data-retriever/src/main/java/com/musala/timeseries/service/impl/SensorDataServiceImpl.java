package com.musala.timeseries.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.musala.timeseries.dto.SensorDataDto;
import com.musala.timeseries.entities.SensorData;
import com.musala.timeseries.mapper.SensorDataModelMapper;
import com.musala.timeseries.repository.SensorDataRepository;
import com.musala.timeseries.service.SensorDataService;

@Service
public class SensorDataServiceImpl implements SensorDataService {

	private final SensorDataRepository sensorDataRepository;
	private final SensorDataModelMapper sensorDataModelMapper;

	@Autowired
	public SensorDataServiceImpl(SensorDataRepository sensorDataRepository,
			SensorDataModelMapper sensorDataModelMapper) {
		super();
		this.sensorDataRepository = sensorDataRepository;
		this.sensorDataModelMapper = sensorDataModelMapper;
	}

	@Override
	public void saveSensorData(SensorDataDto sensorDataDto) {
		final SensorData sensorData = this.sensorDataModelMapper.map(sensorDataDto);

		this.sensorDataRepository.insert(sensorData);
	}
}
