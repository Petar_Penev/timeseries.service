package com.musala.timeseries.listener;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.musala.timeseries.dto.SensorDataDto;
import com.musala.timeseries.service.SensorDataService;

@Component
public class Consumer {
	private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);

	private final ObjectMapper objectMapper;
	private final SensorDataService sensorDataService;

	@Autowired
	public Consumer(SensorDataService sensorDataService, ObjectMapper objectMapper) {
		this.sensorDataService = sensorDataService;
		this.objectMapper = objectMapper;
	}

	/**
	 * JMS Listener configured to listen the a JMS queue for sensor data and inserts
	 * in in the database
	 * 
	 * @param message
	 *            The message coming from the JMS queue
	 * @throws InterruptedException
	 */
	@JmsListener(destination = "${spring.activemq.queue}", concurrency = "${spring.activemq.concurrency}")
	public void receiveQueue(String message) throws InterruptedException {
		try {
			final SensorDataDto sensorData = this.objectMapper.readValue(message, SensorDataDto.class);
			sensorDataService.saveSensorData(sensorData);
		} catch (final IOException e) {
			LOGGER.error("Exception occured while trying to deserialize this message [{}] from queue.", message, e);
		}

	}
}
