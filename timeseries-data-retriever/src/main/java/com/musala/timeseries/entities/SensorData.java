package com.musala.timeseries.entities;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

import lombok.Data;

@Data
@Table("sensorData")
public class SensorData {
	@PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
	private String name;

	@PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
	private long time;

	private Double value;
}
