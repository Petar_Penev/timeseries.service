package com.musala.timeseries.config;

import javax.jms.Queue;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

@Configuration
@EnableJms
public class JmsConfig {

	@Value("${spring.activemq.queue}")
	private String queueName;

	@Bean
	public Queue queue() {
		return new ActiveMQQueue(queueName);
	}
}
