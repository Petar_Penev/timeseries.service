package com.musala.timeseries.mapper;

import com.musala.timeseries.dto.SensorDataDto;
import com.musala.timeseries.entities.SensorData;

public interface SensorDataModelMapper {
	SensorData map(SensorDataDto sensorDataDto);
}
