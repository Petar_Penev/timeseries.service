package com.musala.timeseries.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.musala.timeseries.dto.SensorDataDto;
import com.musala.timeseries.entities.SensorData;
import com.musala.timeseries.mapper.SensorDataModelMapper;

@Component
public class SensorDataModelMapperImpl implements SensorDataModelMapper {

	private final ModelMapper modelMapper;

	@Autowired
	public SensorDataModelMapperImpl(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

	@Override
	public SensorData map(SensorDataDto sensorDataDto) {
		return modelMapper.map(sensorDataDto, SensorData.class);
	}

}
