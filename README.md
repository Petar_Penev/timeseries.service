## Architecture

![Architecture](TimeseriesService.png)

---

## Architecture Components

1. Gateway service - to deal with the different IoT devices, their communication channels and to unify the data of the sensors.
2. Message Broker - fast and reliable connection between the gateway and the data retriever service.
3. Data retriever service - stateless service which should collect and save the timeseries data in the Cassandra database.
4. Cassandra database - to store all timeseries data
5. MySQL database - to store user identities and sensor details
6. Rollup service - to roll up the timeseries data stored every second to two more tables with precision 1 houd and 1 day
7. WebService - secured web service which gives the opportunity to see the aggregated data of the sensors

---

## Installation

*Disclaimer: the current setup is on Windows machine there could be some differences for the different operation systems*

**Prerequisites:** Installed docker


**1. Install & Setup Cassandra Server**

Open terminal/command prompt

Execute the below command to pull and run Cassandra container:
```
docker run -d -p 9042:9042 --name timeseries_cassandra Cassandra
```

Connect to the newly created container:
```
docker exec -it timeseries_cassandra bash
```

Open Cassandra's cqlsh tool to create a keyspace for the services by executing the below command:
```
cqlsh
```

Once you enter the cqlsh tool execute the below query to create the needed keyspace:
```
CREATE  KEYSPACE sensor_timeseries WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };
```

Exit the container with exit command (twice once for cqlsh tool and once for the container) or just open new command prompt/terminal

**2. Install Message Broker**

Execute the below command to pull and run ActiveMQ container:
```
docker run -d -p 61616:61616 -p 8161:8161 --name timeseries_activemq rmohr/activemq
```

**3. Install & Setup MySQL Server**

Execute the below command to pull and run MySQL container:
```
docker run -d -p 3306:3306 --name=timeseries_mysql mysql/mysql-server
```

Wait few seconds and execute the below command to obtain the automatically created mysql password
```
docker logs timeseries_mysql 2>&1 | grep GENERATED
```

You should see the below row in the command prompt as a result of the above command, if you don't see it, just wait few more seconds 
and execute the command again

[Entrypoint] GENERATED ROOT PASSWORD: ynPIMiv@vNAt#ajAz-Ek8@P-@sF

To login in the database use the below command and for password use the password obtained by the above command:
```
docker exec -it timeseries_mysql mysql -u root -p
```

Change the default password of the database root user by executing the below command:
```
ALTER USER 'root'@'localhost' IDENTIFIED BY 'passw0rd';

Create user and database which should be used by the Timeseries services by executing the below commands:
CREATE USER 'timeseries'@'%' IDENTIFIED BY 'timeseriesPassw0rd';
GRANT ALL PRIVILEGES ON *.* TO 'timeseries'@'%' WITH GRANT OPTION;
create database timeseries;
grant all privileges on timeseries.* to timeseries;
```

Start the com.musala.timeseries.sensor.service and wait to start. The hibernate will migrate the database tables.

Once the service is already started execute the below queries to fill the database with the beginning dummy data:
(You can use the command prompt with the already opened mysql tool or you can connect to the database on localhost:3306 with you favourite tool)
```
use timeseries;

insert into users(username,password,enabled) values('admin','$2a$10$ue0mLCHcWyxjuIQZK1S/ZeE8vkwJXBxWzENfZvdoBCW5JjcQ41Djy',true);
insert into authorities(username,authority) values('admin','ROLE_ADMIN');
	
insert into users(username,password,enabled) values('omnipotent', '$2a$10$7qAhZoYyItl5XmaHzq4qheYKZy1HRNEbIK8mzV2H02dZRZ9Qq2uRm', true);
insert into authorities(username, authority) values('omnipotent', 'SENSOR_ADD');
insert into authorities(username, authority) values('omnipotent', 'SENSOR_DATA');
insert into authorities(username, authority) values('omnipotent', 'ROLE_ADMIN');

insert into sensors(name, type, unit, user) values ('thermostat_1', 'thermostat', 'C', 'omnipotent');
insert into sensors(name, type, unit, user) values ('thermostat_2', 'thermostat', 'C', 'omnipotent');
insert into sensors(name, type, unit, user) values ('car_fuel_1', 'car_fuel_monitor', 'L', 'omnipotent');
insert into sensors(name, type, unit, user) values ('hrm_1', 'hear_rate_meter', 'BPS', 'omnipotent');
insert into sensors(name, type, unit, user) values ('hrm_2', 'hear_rate_meter', 'BPS', 'omnipotent');
```

**4. Run all other services**

run service: com.musala.timeseries.sensor.gateway
run service: com.musala.timeseries.sensor.data.retriever
run service: com.musala.timeseries.sensor.rollup

**5. To sign in the system you can use one of the below credentials**

Login as admin with admin only permissions:
admin/53cur3d_Passw0rd

Login as omnipotent user which has all roles and authorities:
omnipotent/pass

**6. For easier access to the WebService you can use the Postman collection from [here](Timeseries.postman_collection.json)**

Also documentation for the WebService can be found on address http://localhost:8080/timeseries/api/swagger-ui.html

---

## Scope 

I tried to implement all requirements in the assignment and I think that I've covered most of them. 

Currently there is a simulation of around 100 sensors and the system is working fine with this load on my local machine. 

There are few types of sensors supported - hear rate meter, car fuel monitor and thermostat but we can add more types easily. The idea
behind the types was very good but I didn't have enough time to implement it. My idea was to have metadata for the different sensors on their creation
so the gateway can read it and know what is the format of the incoming data of the sensor and how to transform it to the unified SensorData. 
Also the web service could use the metadata to see what is the unit of the value and transform it if it's needed. 

Anyway, we have a working prototype with the implementation of the main target requirements -> simulation of few or more sensors, collect 
the data of the sensors and have secured web service which can return aggregations over the data.

Unfortunately there wasn't enough time to make some Unit/Integration tests. Also the Gateway service is partially mocked because of the tight time.

### Scalability:

* The Gateway service can be scaled very easily because it's stateless.
* The Retriever service is stateless too so it can be spawned multiple times or the threads of it's JMS consumer can be tuned for better results.
* Cassandra database - it can be clustered easily and provide very good writing/reading times.
* Rollup service - provides us the possibility to query aggregated data for big time-frames very fast.
* The Web Service is standalone service which is good because it can be deployed in a separate server and work independently of the load
of the system also it's stateless so it can be multiplied
	

### Restrictions
	
* There are three calculation precision levels - finest, hourly, daily. This means that in finest mode you can get the data for a period with 
precision of second, but in the other modes with precision of hour or day. For example for hourly precision you can get the data from 12.03.2018 
at 06:00 to 13.03.2018 at 17:00 but not more precisely.
* Finest precision can be used only for a time periods that are in the last week. If the user wants more older data he should use hourly or daily
precision.
* No clear strategy how to scale the Rollup service - currently the most easy way to scale is with threadpool + async queries to the Cassandra (not implemented)
* Theoretically one Cassandra table can keep 2 billion cells so currently our table with unprocessed timeseries data have time to live
604800 (1 week) and 3 columns which means that 1 sensor can get 2M of capacity per week therefore we can have maximum of 1 000 sensors with the
current settings.

